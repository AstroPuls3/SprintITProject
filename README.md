# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

****
* Creating Backlog items requirements:

* Backlog cards need to be S.M.A.R.T
 
 * S - Specific: What specifically will this achieve

 * M - Measurable: Can the progress I make on this card be measured? 

 * A - Achievable: Can I complete this card in the 2 week sprint?

 * R - Relevant: Is this backlog item relevant to the overall goal of the project

 * T - Time Bound: All backlog items need to be able to be completed in a 2 week sprint. 

 ****


 ****
 Project Proposal Link

 * https://docs.google.com/document/d/1DDsxoXcR1qM0OD6Dn6ruK-y5agZvnqHvwuQkJO3RD_w/edit?usp=sharing

 ****
 Git Cheat sheet

 * https://gist.github.com/hofmannsven/6814451

 ****