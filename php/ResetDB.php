<?php
require "connectDB.php";
$intial_users_data = [
    ["Arthur","Dawkins","dawkinsarthursir@gmail.com","arthur3","arthur1234",'ChIJRaJiKJ7Aj1QR1vSoxJSpcnI'],
    ["Samantha","Sonnie","sonniesamantha@gmail.com","samantha3","sonnie1234",'ChIJ_fTFXPaxj1QRqCUhuV4CqNE'],
    ["Brandon","Evans","evansbrandon@gmail.com","brandon3","brandon1234",'ChIJ_fTFXPaxj1QRqCUhuV4CqNE'],
    ["Michelle","Robertson","robertsonmichelle@gmail.com","michelle3","michelle1234",'ChIJU7wrquyXkVQR4ieu-8uwux4'],
    ["David","Robertson","robertsondavid@gmail.com","david3","david1234",'ChIJU7wrquyXkVQR4ieu-8uwux4'],
    ["Kevin","Dawkins","dawkinskevin@gmail.com","kevin3","kevin1234",'ChIJ0SPsQq9lk1QRIeD5G-Yyv8Y'],
    ["Ellen","Carey","CareyEllen@gmail.com","ellen3","ellen1234",'ChIJ0SPsQq9lk1QRIeD5G-Yyv8Y'],
    ["Donald","Carey","Careydonald@gmail.com","donald3","donald1234",'ChIJf48v7Qp1kVQR_z4Wxoighuo'],
    ["Conner","Richerdson","richerdsonconner@gmail.com","conner3","conner1234",'ChIJN1llbiZ6hVQR4GJymvdzzHI'],
];

$intial_dogs_data = [
    ["Duke","High","Medium","https://www.shepped.com/wp-content/uploads/2015/05/german-shepherd-mix.jpg","1",'1'],
    ["Molly","Low","Medium","http://cdn1-www.dogtime.com/assets/uploads/2010/07/file_808_column_what-are-the-advantages-of-getting-a-senior-dog.jpg","1",'0'],
    ["Dasiy","High","Large","http://www.mountaintailsphotography.com/wp-content/uploads/2013/04/PUPPY.jpg","1",'1'],
    ["Echo","High","Small","http://www.dogbreedslist.info/uploads/allimg/dog-pictures/Pomeranian-1.jpg","3",'1'],
    ["Luna","Low","Xsmall","https://cdn.petcarerx.com/LPPE/images/articlethumbs/white-dogs-Small.jpg","6",'1'],
    ["Slippie","High","Xlarge","http://www.dogbreedslist.info/uploads/allimg/dog-pictures/American-Mastiff-1.jpg","5",'1'],
    ["Brondo","Medium","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRevM0nUZvICcWEpEUlbNH2QqRO6P1QMH6_cejoGw_TRn49iD16","4",'1'],
    ["Dozer","High","Medium","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYocjNduLu-kS2VgVgFPZwNG5ECyKzMezItn25ceoBcjMGuDn0","3",'1'],
    ["Nala","Low","Small","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXbeOc4iEqdoNflMjdH7WjCo6ty2OyrJDctQSlsU-cagEMT8X5EQ","2",'1'],
    ["Bella","Medium","Medium","https://howtotrainthedog.com/wp-content/uploads/2018/01/shepherd-inu.jpg","2",'1'],
    ["Rudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYM878EuRqgpaO7XNC0z3a3ci-BLWVmKQ3PjfA9fF4IeLiPAlMxg","8",'0'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","9",'1'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","2",'1'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","2",'1'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","2",'1'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","2",'1'],
    ["Trudy","High","Large","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9L0zcWvdQQHGHkxAAEkfnK4v1IAF9HAEaTQ3xo-7g2RUjY-1sIw","2",'1'],
];

$dogParkData = [
['ChIJRaJiKJ7Aj1QR1vSoxJSpcnI','Sequim Dog Park','Sequim Dog Park, 202 N Blake Ave, Sequim, WA 98382, USA',	'(48.0809217, -123.08358709999999)','https://lh5.googleusercontent.com/p/AF1QipPEPNpLtq9q9oF37hk4Gwyk0t-SAQNdQIZT2DN_=w408-h229-k-no'],
['ChIJ_fTFXPaxj1QRqCUhuV4CqNE','Port Angeles Dog Park','Port Angeles Dog Park, 1469-1737 W Lauridsen Blvd, Port Angeles, WA 98363, USA','(48.1141224, -123.48040779999997)','https://lh5.googleusercontent.com/p/AF1QipNaVnWUREUYug2GxUXfzVJbtELVw7Ro29Ca7tG2=w408-h229-k-no'],
['ChIJU7wrquyXkVQR4ieu-8uwux4','Kneeland Park','Kneeland Park, 100 Turner Ave, Shelton, WA 98584, USA',	'(47.208638, -123.101785)',	'http://www.willhiteweb.com/washington/shelton/imagination-station-kneeland.jpg'],
['ChIJ0SPsQq9lk1QRIeD5G-Yyv8Y',	'Warrenton Dog Park',	'Warrenton Dog Park, NW Warrenton Dr, Hammond, OR 97121, USA',	'(46.190968, -123.92959050000002)',	'https://lh5.googleusercontent.com/p/AF1QipO-yRpjvnGU7lHsotH2uY-cjvz80v4QPvGb4VEI=w408-h229-k-no'],
['ChIJf48v7Qp1kVQR_z4Wxoighuo',	'Sunrise Park',	'Sunrise Park, 505 Bing St NW, Olympia, WA 98502, USA',	'(47.0496372, -122.929014)',	'https://www.ankenyiowa.gov/Home/ShowImage?id=440&t=635265800482100000'],
['ChIJAcR9bWVgkVQRmx6fJSPGQ8w',	'Fort Borst Park',	'Fort Borst Park, Centralia, WA 98531, USA',	'(46.7217284, -122.98176999999998)',	'http://www.lewistalk.com/wp-content/uploads/2015/10/Borst-Park-Centralia-Washington-6.jpg'],
['ChIJKx82Eg5rlFQRn3wk4jEfCjw',	'Gearhart Gardens Dog Park',	'Gearhart Gardens Dog Park, 200 Freedom Way, Longview, WA 98632, USA',	'(46.1084599, -122.89462309999999)',	'http://media.oregonlive.com/travel_impact/photo/gearhartdogjpg-4104955ba08003db.jpg'],
['ChIJwdVNY1dUlFQRlEsiAdlll48',	'Scappoose Dog Park',	'Scappoose Dog Park, 52590 Captain Roger Kucera Way, Scappoose, OR 97056, USA',	'(45.7601255, -122.8838953)',	'https://lh5.googleusercontent.com/p/AF1QipNGT7z67kC_XveueuOX2fffNrjL-B6xFIcwHW8o=w408-h229-k-no'],
['ChIJk-XNnQxihVQR_ohUxZYx6yA',	'Clover Valley Off Leash Park',	'Clover Valley Off Leash Park, Ault Field Rd, Oak Harbor, WA 98277, USA',	'(48.3262146, -122.65761520000001)',	'https://lh5.googleusercontent.com/p/AF1QipNoxQZkUyDxPm8YZeR4tupL9ReVtxOuq61zQr2I=w408-h229-k-no'],
['ChIJoRju6ANihVQRRR5gTU2cFwE',	'Technical Drive Off-leash Dog Park',	'Technical Drive Off-leash Dog Park, 501 Technical Dr, Oak Harbor, WA 98277, USA',	'(48.31543869999999, -122.64248939999999)',	'https://lh5.googleusercontent.com/p/AF1QipP_QnJV9UW8H4X2ukSE5MJUvflZuJnDoktNPJPB=w408-h725-k-no'],
['ChIJN1llbiZ6hVQR4GJymvdzzHI',	'Ace Of Hearts Rotary Park',	'Ace Of Hearts Rotary Park, 3901 H Ave, Anacortes, WA 98221, USA',	'(48.4908369, -122.62441580000001)',	'https://lh5.googleusercontent.com/p/AF1QipPvmIoMGJfILsSo2igkJC3BrAaS6BLXL4YDT2Jl=w408-h306-k-no'],
['ChIJKYYN35v0j1QRcKZqgy_rWhY',	'Patmore Pit',	'Patmore Pit, 497 Patmore Rd, Coupeville, WA 98239, USA',	'(48.1945766, -122.64115200000003)',	'http://www.fetchparks.org/images/patmore1.jpg'],
['ChIJ7QSyg9o6kFQRUgxuhXwTYTk',	'Silverdale Dog Park',	'Silverdale Dog Park, 11601 Silverdale Way NW, Silverdale, WA 98383, USA',	'(47.667875, -122.68186600000001)',	'https://lh5.googleusercontent.com/p/AF1QipOGNnzvCCxWMP-CkVcvgKRL9hTj4TwmKsLcEiex=w408-h229-k-no'],
['ChIJRbGRZw0jkFQRGehuJPy5D98',	'Frank Raab Park',	'Frank Raab Municipal Park, 18349 Caldart Ave NE, Poulsbo, WA 98370, USA',	'(47.72935349999999, -122.6297821)',	'https://visitpoulsbo.com/wp-content/uploads/2018/02/raab-park.jpg'],
['ChIJ9V3w6cALkVQRtlB4DgLQAmg',	'Off-Leash Dog Park',	'Off-Leash Dog Park, 2420 Hogum Bay Rd NE, Lacey, WA 98516, USA',	'(47.06589659999999, -122.7626459)',	'https://lh5.googleusercontent.com/p/AF1QipNSDh8tKpokdouRJBIfGeajOpuobmSHEeuM18lX=w408-h306-k-no'],
['ChIJ77I0IsELkVQR_niLDT7DiiE',	'Closed Loop Park',	'Closed Loop Park, 2418 Hogum Bay Rd NE Exd, Olympia, WA 98516, USA',	'(47.0665184, -122.7615763)',	'https://lh5.googleusercontent.com/p/AF1QipNfrNavOklTvzYss5NN5w93I9tnb4ZLV2h6FvyU=w408-h306-k-no'],
['ChIJIQBJ8ZUIkVQRprjQfHIanyE',	'Dupont Powderworks Park',	'Dupont Powderworks Park, 1775 Bobs Hollow Ln, DuPont, WA 98327, USA',	'(47.098844, -122.64924300000001)',	'https://lh5.googleusercontent.com/p/AF1QipPxWuTvPEPn6q4wPr_53GdmLOlSZYlyl_GHe3II=w408-h408-k-no'],
['ChIJk7TKXx6vlVQRlXfe4A1cXGo',	'Ross Dog Park',	'Ross Dog Park, 5167 NE 15th Ave, Vancouver, WA 98663, USA',	'(45.6583717, -122.65640669999999)',	'https://lh5.googleusercontent.com/p/AF1QipOPxrWAgXH9JmakhozmGLV80EOj26-_IEuTXFh9=w408-h306-k-no'],
['ChIJYV6YZQ2jhVQR71pLs1qeanU',	'Squalicum Dog Park',	'Squalicum Dog Park, 1001 Squalicum Way, Bellingham, WA 98225, USA',	'(48.7685931, -122.49946499999999)',	'https://lh5.googleusercontent.com/p/AF1QipPyFMafRh5BBuxeU5LLlvBSdSl1loeRFs7qrhmF=w408-h306-k-no'],
['ChIJ_cS9-RqjhVQRGj2A4MrSKHQ',	'Little Squalicum Park',	'Little Squalicum Park, 640 Marine Dr, Bellingham, WA 98225, USA',	'(48.76708, -122.51628900000003)',	'https://www.cob.org/photos/Services/Recreation/parks-trails/little/little-squalicum-beach.JPG'],
['ChIJjYCpKGH8j1QRQVXYVSEkAoQ',	'Marguerite Brons Memorial Park',	'Marguerite Brons Memorial Park, 2837 Becker Rd, Clinton, WA 98236, USA',	'(47.9948369, -122.45962589999999)',	'https://lh5.googleusercontent.com/p/AF1QipNS4zp_t45rAMe6RwKh145m9kTwatT34x8ptAAV=w408-h229-k-no'],
['ChIJiajv0gNIkFQRnpY7444VW0M',	'Howe Farm County Park',	'Howe Farm County Park, 1901 Long Lake Rd SE, Port Orchard, WA 98366, USA',	'(47.5316674, -122.57985450000001)',	'https://img.thrfun.com/images/articles22/magshad3.jpg'],
['ChIJzxyROmlTkFQR3On73a_M-LI',	'Point Defiance Dog Park',	'Point Defiance Dog Park, Five mile Rd, Tacoma, WA 98407, USA',	'(47.3023618, -122.53258289999997)',	'https://lh5.googleusercontent.com/p/AF1QipPkXdjrysN6q4p3aYsG3wSh1UQW_Xewq3jtKphZ=w408-h306-k-no'],
['ChIJLxhYZlarkVQRY_AkzP0K_A4',	'Tubbys Trail Dog Park',	'Tubbys Trail Dog Park, 1701 14th Ave NW, Gig Harbor, WA 98335, USA',	'(47.27787420000001, -122.5588148)',	'http://activerain.com/image_store/uploads/agents/bradyhoward/files/20150215_144236.jpg'],
['ChIJv-4PpyYAkVQRfWuXQgn5Swg',	'Wapato Park Off-Leash Dog Park',	'Wapato Park Off-Leash Dog Park, 6802 S Sheridan Ave, Tacoma, WA 98408, USA',	'(47.19700599999999, -122.45440680000002)',	'https://lh5.googleusercontent.com/p/AF1QipN2f7eGEfr3dTI2_2tIs_xruVolG6qk8iXPCK9F=w408-h229-k-no'],
['ChIJn4vAeID_kFQRLflSNI0eLAk',	'McKinley Park',	'McKinley Park, 907 Upper Park St, Tacoma, WA 98404, USA',	'(47.2353645, -122.4213441)',	'https://www.nycgovparks.org/photo_gallery/full_size/19126.jpg'],
['ChIJMWTleMoAkVQR2OYt2900NL4',	'Fort Steilacoom Park',	'Fort Steilacoom Park, 8714 87th Ave SW, Lakewood, WA 98498, USA',	'(47.173507, -122.5561169)',	'http://www.willhiteweb.com/puget_sound_parks/fort_steilacoom_park/fort_steilacoom_sign.jpg'],
['ChIJi1A9_yQAkVQRtIHmSozVf9o',	'Wapato Park',	'Wapato Park, 6500 S Sheridan Ave, Tacoma, WA 98408, USA',	'(47.1947542, -122.45486289999997)',	'http://www.willhiteweb.com/puget_sound_parks/wapato_lake_park/wapato_lake_park.jpg'],
['ChIJVVVVVVkBkVQRnl1haOiEnJQ',	'Dog Park',	'Dog Park, McChord AFB, WA 98439, USA',	'(47.1239365, -122.48696589999997)',	'https://lh5.googleusercontent.com/p/AF1QipNGX6lZxYS0e7BuJqvauRNWbGZtFYkhEzZTq2Gh=w408-h306-k-no'],
['ChIJKQzf83-xlVQR2IU72iudJd4',	'Lucky Memorial Dog Park',	'Lucky Memorial Dog Park, 10100 NE 149th St, Brush Prairie, WA 98606, USA',	'(45.7292507, -122.56922729999997)',	'https://lh5.googleusercontent.com/p/AF1QipNanrmRzO03CJRP3CeRx92IX8AsZue-ZhIrcKdW=w408-h229-k-no'],
['ChIJX6okKeGwlVQR64_Vztszkcw',	'Kane Memorial Dog Park',	'Kane Memorial Dog Park, 10910 NE 172nd Ave, Vancouver, WA 98682, USA',	'(45.7037368, -122.50691949999998)',	'https://lh5.googleusercontent.com/p/AF1QipMfmnifbTTCdVN08iwMuqi8YZ8hqpp9dyIxHWw=w408-h229-k-no'],
['ChIJgVemNt2wlVQRdu8h1Ge0vIU',	'Hockinson Meadows Community Park',	'Hockinson Meadows Community Park, 10910 NE 172nd Ave, Vancouver, WA 98682, USA',	'(45.70308120000001, -122.49878160000003)',	'https://s3-media3.fl.yelpcdn.com/bphoto/EYofy0QJn8qbXbWLwk9iqQ/o.jpg'],
['ChIJZZ3H_6ehhVQRLoJA_DqJOao',	'Lake Padden Park',	'Lake Padden Park, 4882 S Samish Way, Bellingham, WA 98229, USA',	'(48.7059866, -122.4555929)',	'https://www.cob.org/photos/Services/Recreation/parks-trails/lake-padden/lake-padden-boat-launch.JPG'],
['ChIJ-TSBp0ABkFQRE_UmeMiiQUQ',	'Loganberry Lane Park',	'Loganberry Lane Park, 9201 18th Ave W, Everett, WA 98204, USA',	'(47.9156781, -122.26058390000003)',	'https://s3-media2.fl.yelpcdn.com/bphoto/I-xarn_21B3oxQbNYFWBzA/l.jpg'],
['ChIJCZC4ImgAkFQRMeB-GPx9KH0',	'Howarth Park',	'Howarth Park, 1127 Olympic Blvd, Everett, WA 98203, USA',	'(47.9634844, -122.24001609999999)',	'https://chittisterchildren.files.wordpress.com/2011/06/dscn5939.jpg'],
['ChIJTV05twYQkFQRTtKBgzxU6qM',	'Mountlake Terrace Off-Leash Dog Park',	'Mountlake Terrace Off-Leash Dog Park, 5303 228th St SW, Mountlake Terrace, WA 98043, USA',	'(47.7935344, -122.3042216)',	'https://www.cityofmlt.com/ImageRepository/Path?filePath=%2Fdocuments%5CIntranet%2FIMG00665.jpg'],
['ChIJT_0Ou1AakFQRsJ1Vzddoqg8',	'Off Leash Area Edmonds',	'Off Leash Area Edmonds, 498 Admiral Way, Edmonds, WA 98020, USA',	'(47.8043223, -122.39397959999997)',	'https://lh5.googleusercontent.com/p/AF1QipMcH0wQgPgkoBaCgm-xIbXJOktu2qqUhYBHkguc=w408-h306-k-no'],
['ChIJnbC8WMQQkFQRX1etNM2cEow',	'Shoreview Park Off Leash Dog Park',	'Shoreview Park Off Leash Dog Park, 320 NW Innis Arden Way, Seattle, WA 98177, USA',	'(47.7504805, -122.36393090000001)',	'http://www.doggoes.com/sites/default/files/shoreline_dogpark2.jpg'],
['ChIJvd1wIAkRkFQRj3llCpGDZ7M',	'Eastside Off-Leash Dog Area',	'Eastside Off-Leash Dog Area, 1902 NE 150th St, Shoreline, WA 98155, USA',	'(47.7392711, -122.30788059999998)',	'https://lh5.googleusercontent.com/p/AF1QipNCr6J-lponSxthhCYe2JSI3eeLNIE4DMrzFH5V=w408-h306-k-no'],
['ChIJDdEp4kUUkFQRYgiQJxQ7Akc',	'Lower Woodland Off Leash Area',	'Lower Woodland Off Leash Area, 1000 N 50th St, Seattle, WA 98103, USA',	'(47.6701819, -122.34447620000003)',	'https://lh5.googleusercontent.com/p/AF1QipOphBZR3tWsKE_Z8UnKS09QGFi_zg-U4zvbNmW9=w408-h306-k-no'],
['ChIJXW9gXCoFkFQRqQPQXUpf_a0',	'Lynndale Off-leash Dog Area',	'Lynndale Off-leash Dog Area, 7524-7528 Olympic View Dr, Edmonds, WA 98026, USA',	'(47.8309734, -122.33535469999998)',	'https://lh5.googleusercontent.com/p/AF1QipM0pFV3qXmx_FpNE38_54Pl1Cjar6jtrhQSDq8r=w408-h209-k-no'],
['ChIJm5uTNEERkFQROLUe_pnFQz0',	'Northacres Off-Leash Dog Area',	'Northacres Off-Leash Dog Area, 12718 1st Ave NE, Seattle, WA 98125, USA',	'(47.7221544, -122.32570929999997)',	'https://lh5.googleusercontent.com/p/AF1QipP6oLKF5zXoa-zIcNCuosNqSHGxiqH9Aa_jLtfh=w408-h306-k-no'],
['ChIJi6FQmqcTkFQRy7BqppG5Q4Q',	'Warren G. Magnuson Park Off Leash Area',	'Warren G. Magnuson Park Off Leash Area, 7400 Sand Point Way NE, Seattle, WA 98115, USA',	'(47.6828973, -122.25638019999997)',	'https://lh5.googleusercontent.com/p/AF1QipM9HLscHCVSvwWcYZxXiiM_jpBk264JLtLfA5zc=w408-h229-k-no'],
['ChIJnbC8WMQQkFQROUrS_Zjzvg0',	'Shoreline Community College Off Leash Dog Park',	'Shoreline Community College Off Leash Dog Park, 700 NW Innis Arden Way, Seattle, WA 98177, USA',	'(47.7493312, -122.36415590000001)',	'https://lh5.googleusercontent.com/p/AF1QipPIErFAZibwGw-x0LWnWt43F3D0BxDQUNje6irs=w408-h306-k-no'],
['ChIJU3VYlJAVkFQR_gQPJzwFf4w',	'Magnolia Manor Park',	'Magnolia Manor Park, 3500 28th Ave W, Seattle, WA 98199, USA',	'(47.65139449999999, -122.39133670000001)',	'https://lh5.googleusercontent.com/p/AF1QipOFuMUwyjDHncaAjQadPLMqsEytQKQThCgwW_L_=w408-h229-k-no'],
['ChIJafU2WaQQkFQR4oI9uVn9-Ok',	'Richmond Beach Saltwater Park',	'Richmond Beach Saltwater Park, 2021 NW 190th St, Shoreline, WA 98177, USA',	'(47.7658449, -122.38339710000002)',	'https://i.pinimg.com/originals/c3/bb/d7/c3bbd7dc392a73e854f2ac9f820dee91.jpg'],
['ChIJTYbqqJlEkFQR1VPqX0dNrX4',	'Burien Off Leash Dog Park',	'Burien Off Leash Dog Park, 15810 5th Ave SW, Burien, WA 98166, USA',	'(47.46115959999999, -122.341453)',	'http://b-townblog.com/wp-content/uploads/2016/09/1-park.jpg'],
['ChIJrYcj-31BkFQREeYiQ61hMvk',	'Westcrest Park',	'Westcrest Park, 9000 8th Ave SW, Seattle, WA 98106, USA',	'(47.521375, -122.34338500000001)',	'https://lh5.googleusercontent.com/p/AF1QipOCk0HLP2dFQXDmMHQcHLJVVOp1C2aBRArM8ISq=w408-h306-k-no'],
['ChIJB3UJKPNqkFQRsUZH_0KGfbM',	'Blue Dog Pond',	'Blue Dog Pond, 1520 26th Ave S, Seattle, WA 98144, USA',	'(47.5887416, -122.29856960000001)',	'https://lh5.googleusercontent.com/p/AF1QipP10V-Xf19m17EsZwe05B3KbOSD8mc_OC0I4Hte=w408-h270-k-no'],
['ChIJeYOGOUwVkFQRd4uMWCajifo',	'Regrade Park',	'Regrade Park, 2251 3rd Ave, Seattle, WA 98121, USA',	'(47.614499, -122.34478710000002)',	'https://lh5.googleusercontent.com/p/AF1QipMdAj1eTTanfM5OPJnKcZKAMCHTdW9kkGuruNha=w408-h306-k-no'],
['ChIJS1MEvcpqkFQRbVlqirSAwNE',	'Plymouth Pillars Park',	'Plymouth Pillars Park, 1050 Pike St, Seattle, WA 98101, USA',	'(47.613634, -122.3293956)',	'http://youdidwhatwithyourweiner.com/wp-content/uploads/2011/04/dsc_0041A.jpg'],
['ChIJva6zWb1bkFQRo_prR-FW8mw',	'Grandview Off-Leash Dog Park',	'Grandview Off-Leash Dog Park, 3507 S 228th St, SeaTac, WA 98198, USA',	'(47.397564, -122.28570430000002)',	'http://www.doggoes.com/sites/default/files/dog_pics/yorkton_081_0.jpg'],
['ChIJrb6zgGgVkFQR-xJrNqBgWjc',	'Kinnear Park',	'Kinnear Park, 899 W Olympic Pl, Seattle, WA 98119, USA',	'(47.627315, -122.36688200000003)',	'https://www.seattle.gov/images/Departments/ParksAndRecreation/Parks/JKL/KinnearPark1.jpg'],
['ChIJC8GgZL1rkFQRZhoiyuxpGdw',	'Luther Burbank Park',	'Luther Burbank Park, 2040 84th Ave SE, Mercer Island, WA 98040, USA',	'(47.5911806, -122.22566849999998)',	'https://lh5.googleusercontent.com/p/AF1QipMS_pDz2npbHBDRiYAEJx2rW0ocLYKjxpUxIyeb=w408-h306-k-no'],
['ChIJha4r7yAVkFQR8--AC9T-6ns',	'I-5 Colonnade',	'I-5 Colonnade, 1701 Lakeview Blvd E, Seattle, WA 98102, USA',	'(47.634874, -122.32393990000003)',	'https://www.evergreenmtb.org/images/trail/58/colonnade-wide.jpg'],
['ChIJjUdVA57-kFQRx35wFCczyCI',	'Clarks Creek Park',	'Clarks Creek Park, 12th Avenue Southwest & 17th Street Southwest, Puyallup, WA 98371, USA',	'(47.1806448, -122.31714620000002)',	'https://wdfw.wa.gov/fishing/salmon/chum/graphics/clarkbridge.jpg'],
['ChIJbeQcdntXkFQRLit52PVuy40',	'French lake Dog Park',	'French lake Dog Park, 31531 1st Ave S, Federal Way, WA 98003, USA',	'(47.3192686, -122.3354592)',	'https://lh5.googleusercontent.com/p/AF1QipNTa8cKU87lBdDajAIM1iJi3hgVS0W1lmeQowez=w408-h306-k-no'],
['ChIJ6-x794H_kFQRDp_CRE6EdQU',	'Rogers Park',	'Rogers Park, 3151 E L St, Tacoma, WA 98404, USA',	'(47.233109, -122.41454390000001)',	'http://www.dreamtown.com/photos/neighborhoods/west-rogers-park/west-rogers-park-12.jpg'],
['ChIJd1BAIL0BkFQRfE_tIebpl2o',	'Tails and Trails Dog Park',	'Tails and Trails Dog Park, 1301 5th St, Mukilteo, WA 98275, USA',	'(47.9464725, -122.29223819999999)',	'https://lh5.googleusercontent.com/p/AF1QipMf_e7jn-caQAQk_2qHTlxfbKMayUNhcUlmBc4E=w408-h306-k-no'],
['ChIJJ_7ON5gakFQR3_GwPLr0Smw',	'Esperance Park',	'Esperance Park, 7830 222nd St SW, Edmonds, WA 98026, USA',	'(47.7967841, -122.33952290000002)',	'http://www.snohomish.org/images/sized/content/uploads/featured_images/esperance_park-345x216.JPG'],
['ChIJZ34E7zIOkFQRdLhXh3KvRtY',	'Wallace Swamp Creek Park',	'Wallace Swamp Creek Park, 2006, 19851 73rd Ave NE, Kenmore, WA 98028, USA',	'(47.7724038, -122.24441179999997)',	'http://www.willhiteweb.com/washington/kenmore/wallace-swamp-creek-park-bridge.jpg'],
['ChIJMZm7nhgEkFQR0b2oPh5fDDQ',	'Lake Stickney Dog Park',	'Lake Stickney Dog Park, 13521 Manor Way, Lynnwood, WA 98087, USA',	'(47.8755372, -122.26037630000002)',	'https://lh5.googleusercontent.com/p/AF1QipO9aJcHLhDZa32byR8gEO_pVg-B6gCxsbfnXgK2=w408-h229-k-no'],
['ChIJGz6zu3n8kFQRmVsdUVP-_4c',	'Rainier Woods Park',	'Rainier Woods Park, 2610 Cherokee Blvd, Puyallup, WA 98374, USA',	'(47.1658461, -122.25973850000003)',	'https://lh5.googleusercontent.com/p/AF1QipMOwgvOd8_kBXq50beY1MAx9eEJicfdBphJrjM=w408-h306-k-no'],
['ChIJ7cLUtUkVkFQR7vF7ZiDDmfU',	'Denny Park',	'Denny Park, 100 Dexter Ave N, Seattle, WA 98109, USA',	'(47.619053, -122.340936)',	'https://lh5.googleusercontent.com/p/AF1QipOQZRqGL2cpgx1DRq0J1CYkX9FHrX3qr8K6-CUF=w408-h306-k-no'],
['ChIJtZua-5ZqkFQR3KHwDUT1YXk',	'Dr. Jose Rizal Park',	'Dr. Jose Rizal Park, 1007 12th Ave S, Seattle, WA 98144, USA',	'(47.5923129, -122.31784590000001)',	'https://lh5.googleusercontent.com/p/AF1QipO_e5A20hJy4X69WG1EqXCuQkSoKP_TdB39jqLl=w408-h306-k-no'],
['ChIJgUl8-I73kFQR4ST-vyzZ1pc',	'Roegner Park',	'Roegner Park, 601 Oravetz Rd SE, Auburn, WA 98092, USA',	'(47.2693987, -122.22038229999998)',	'http://www.auburnwa.gov/Assets/Parks/AuburnWA/Images/Facility+Rentals/roegner_picnic.jpg'],
['ChIJexwDlQA8kFQR2GN16Pxm308',	'Strawberry Hill Park',	'Strawberry Hill Park, 7666 High School Rd NE, Bainbridge Island, WA 98110, USA',	'(47.638247, -122.54908590000002)',	'https://lh5.googleusercontent.com/p/AF1QipP87onI_3Tx5uchn__bnoFQiFfdT75TzuQVhgWG=w408-h306-k-no'],
['ChIJYRAtipRRhVQRwELtoyLLUJc',	'Strawberry Fields for Rover Off-Leash Park',	'Strawberry Fields for Rover Off-Leash Park, 6100 145th St NE, Marysville, WA 98271, USA',	'(48.1334254, -122.14916929999998)',	'https://lh5.googleusercontent.com/p/AF1QipOU3Nl161fNnIc6poOl_nPexvoW4AMyR7iFX2GO=w408-h225-k-no'],
['ChIJ9R6GQZShhVQRYtZ7IGzs4Wo',	'Arroyo Park',	'Arroyo Park, 1700 Old Samish Rd, Bellingham, WA 98229, USA',	'(48.7012808, -122.48373700000002)',	'https://www.cob.org/photos/Services/Recreation/parks-trails/arroyo/arroyo-park-trail.JPG'],
['ChIJ1wygec8HkFQROOFlUpx00Cg',	'Willis Tucker Off-Leash Dog Area',	'Willis Tucker Off-Leash Dog Area, Snohomish, WA 98296, USA',	'(47.86545, -122.13753750000001)',	'https://igx.4sqi.net/img/general/600x600/wl6g198AfqVtwMaQeHGUcFjhMNDhN8RVRaHRTrQ_le0.jpg'],
['ChIJg4l4p3iqmlQRPPj7EZuBD84',	'Lowell Dog Park',	'Lowell Dog Park, 4502 S 3rd Ave, Everett, WA 98203, USA',	'(47.9580626, -122.19662849999997)',	'https://lh5.googleusercontent.com/p/AF1QipND8cYNCNr_0aQxszQ33zIuB9TNwwsgtfKM_p4B=w408-h229-k-no'],
['ChIJ_1KlUM4HkFQRUr6Qvh90gdE',	'Willis D. Tucker Community Park',	'Willis D. Tucker Community Park, 6705 Puget Park Dr, Snohomish, WA 98296, USA',	'(47.8633643, -122.13960170000001)',	'https://igx.4sqi.net/img/general/600x600/wl6g198AfqVtwMaQeHGUcFjhMNDhN8RVRaHRTrQ_le0.jpg'],
['ChIJSzMo_Z5ykFQRhoM_LBzrPd0',	'Marymoor Off-Leash Dog Park',	'Marymoor Off-Leash Dog Park, 6046 West Lake Sammamish Pkwy NE, Redmond, WA 98052, USA',	'(47.658819, -122.11386729999998)',	'http://www.doggoes.com/sites/default/files/dog_images/PB270046.JPG'],
['ChIJCV9ruX4SkFQR_m_NJoeaYko',	'Jaspers Dog Park',	'Jaspers Dog Park, 11225 NE 120th St, Kirkland, WA 98034, USA',	'(47.706809, -122.19092610000001)',	'https://i.ytimg.com/vi/-W1aYz6E7y4/maxresdefault.jpg'],
['ChIJzbIcSQMGkFQRTKRh6p3hhVU',	'Tambark Creek Park',	'Tambark Creek Park, 17217 35th Ave SE, Bothell, WA 98012, USA',	'(47.8400374, -122.18279000000001)',	'http://carriagepark.net/wp-content/uploads/2014/11/Tambark-Creek-Park.jpg'],
['ChIJcyAiQP9nkFQRxdq3ZdxxkR0',	'Cedar River Trail Off Leash Area',	'Cedar River Trail Off Leash Area, 600 Cedar River Trail, Renton, WA 98057, USA',	'(47.4779767, -122.19467359999999)',	'https://lh5.googleusercontent.com/p/AF1QipMOsAUrOWgSUOLii8x55U5B8LtzVtJD5LnwkRhK=w408-h306-k-no'],
['ChIJNys41ff6kFQRA45YIG7QuLY',	'Viking Park, Off-leash Dog Park',	'Viking Park, Off-leash Dog Park, 18902 82nd St E, Bonney Lake, WA 98391, USA',	'(47.1827956, -122.17658399999999)',	'https://lh5.googleusercontent.com/p/AF1QipOnoX6czvL4WzwP3-UbJ12jeGqYKYi1moDAf5Up=w408-h281-k-no'],
['ChIJ0VcJwCBskFQRGg6Bp9df03A',	'Robinswood Community Park','Robinswood Community Park, 2432 148th Ave SE, Bellevue, WA 98007, USA', '(47.5874, -122.1411)',	'https://u.realgeeks.media/bellevuehomes/landscape/robinswood-neighborhood-bellevue.jpg'],
['ChIJVbbfErtekFQRKml0dnFzvcE',	'Morrill Meadows Park',	'Morrill Meadows Park, 10600 SE 248th St, Kent, WA 98030, USA',	'(47.3801583, -122.19913099999997)',	'https://southsoundparkbench.files.wordpress.com/2013/04/shelter.jpg'],
['ChIJozAE2a-rmlQRnLFvWvjypBE',	'Cavalero Hill Park',	'Cavalero Hill Park, 27032 79th Ave SE, Lake Stevens, WA 98258, USA',	'(47.97539399999999, -122.12656900000002)',	'http://www.snohomish.org/images/sized/content/uploads/featured_images/cavalero-345x216.jpg'],
['ChIJ07SR6QjjkFQRbT9gT7Zp4Oc',	'Orting Dog Park',	'Orting Dog Park, 609 Skinner Way SW, Orting, WA 98360, USA',	'(47.0895775, -122.21022829999998)',	'https://lh5.googleusercontent.com/p/AF1QipOnNARbqm2WoUiR8H2AxPayfy6qsDJauKuxVQM=w408-h228-k-no'],
['ChIJr2qEYw1shVQRxnNUTnh0yGY',	'Bakerview Park',	'Bakerview Park, 3101 E Fir St, Mt Vernon, WA 98273, USA',	'(48.4302292, -122.30001809999999)',	'http://www.doggoes.com/sites/default/files/dog_pics/2_5.jpg'],
['ChIJd8PbjJikmlQR-KFCAN5Dc1g',	'Offleash Dog Park',	'Offleash Dog Park, 703-799 1st St, Sultan, WA 98294, USA',	'(47.8688752, -121.82096860000001)',	'http://cdn0.wideopenpets.com/wp-content/uploads/2016/09/AdobeStock_63759354-770x405.jpg'],
['ChIJ109wK1cakFQRaPf5LgupFXQ',	'Marina Beach Park',	'Marina Beach Park, 470 Admiral Way, Edmonds, WA 98020, USA',	'(47.8050898, -122.393977)',	'https://media-cdn.tripadvisor.com/media/photo-s/0a/45/ca/96/the-kiddos-enjoy-driftwood.jpg'],
['ChIJ-5neRPMNkFQRIq2yooG3gUQ',	'Edith Moulton',	'Edith Moulton, 108th Ave NE & Northeast 137th Place, Kirkland, WA 98034, USA',	'(47.7233393, -122.19668389999998)',	'https://s3-media4.fl.yelpcdn.com/bphoto/mgrKRLSCaY5bMg5gvPWZdg/ls.jpg'],
['ChIJl2dQMb7tj1QRAu-eodAmDYU',	'Chetzemoka Park',	'Chetzemoka Park, Port Townsend, Washington 98368, USA',	'(48.1224953, -122.75542189999999)',	'https://media-cdn.tripadvisor.com/media/photo-s/0a/c1/1d/23/chetzemoka-park.jpg'],
['ChIJtTvSlwgTkFQR48SgKzUZbcM',	'Magnuson Park',	'Magnuson Park, 7400 Sand Point Way NE, Seattle, WA 98115, USA',	'(47.680174, -122.25323779999997)',	'https://lh5.googleusercontent.com/p/AF1QipMl5dCPqoljB8pm0PIXpvNe2RwKWaXsZ1TrvHen=w408-h306-k-no'],
['ChIJCw5mD_s2kFQRH1kuas1n3Ok',	'Pendergast Regional Park',	'Pendergast Regional Park, 1199 Union Ave W, Bremerton, WA 98312, USA',	'(47.5541721, -122.68681270000002)',	'https://i.pinimg.com/236x/a4/74/18/a4741890f490adf226320ced8a5bf878--be-beautiful-kites.jpg'],
['ChIJxdcA5vJqkFQRuzmLIuK8u_8',	'Sam Smith Park',	'Sam Smith Park, 1400 Martin Luther King Jr Way S, Seattle, WA 98144, USA',	'(47.58980700000001, -122.29785700000002)',	'https://www.seattle.gov/images/Departments/ParksAndRecreation/Parks/QRS/SamSmith3.jpg'],
['ChIJC2LWy_17kFQRPXwySKPCoMI',	'Three Forks Off-Leash Dog Park',	'Three Forks Off-Leash Dog Park, Snoqualmie, WA 98065, USA',	'(47.5229771, -121.80375500000002)',	'https://lh5.googleusercontent.com/p/AF1QipPB8yd_jWUpLsC6r5fUCf6DcvPSJb6FLSaQdOJ6=w408-h229-k-no'],
['ChIJTUd7mFZ5kFQRu8VuMOiQcIs',	'Three Forks Natural Area',	'Three Forks Natural Area, 39912 SE Park St, Snoqualmie, WA 98065, USA',	'(47.5221698, -121.7986487)',	'https://www.outdoorproject.com/sites/default/files/styles/cboxshow/public/1403842730/dsc_0001.jpg?itok=UX8i5ZxX'],
['ChIJzWxG_cxxkFQRVIl-r75f_7I',	'Beaver Lake Park',	'Beaver Lake Park, 2600 244th Ave SE, Sammamish, WA 98075, USA',	'(47.5857637, -122.01214670000002)',	'http://www.willhiteweb.com/puget_sound_parks/beaver_lake_park/beaver_lake_park_sign.jpg'],
['ChIJyeIQcZh2kFQRjc-uQqsKM-8',	'Carnation Dog Park',	'Carnation Dog Park, Carnation, WA 98014, USA',	'(47.6467094, -121.91803749999997)',	'https://lh5.googleusercontent.com/p/AF1QipN-fKLlJxrcQglgSDa418x6rXHC4DhZtXF-n-fs=w408-h229-k-no'],
['ChIJqwH0L-NvkFQRcapLaRUBuFc',	'Dog Park',	'Dog Park, Issaquah, WA 98029, USA',	'(47.5550349, -121.99812120000001)',	'http://kcparks.org/wp-content/uploads/2017/05/WTDP-7750.jpg'],
['ChIJswlfgCcLkFQROOivZSsyr2Y',	'Duvall Dog Park',	'Duvall Dog Park, Snoqualmie Valley Trail, Duvall, WA 98019, USA',	'(47.7343802, -121.99062249999997)',	'https://lh5.googleusercontent.com/p/AF1QipNPnk2I6XhOx7STpcMClIefqq9-bqPS2LWg7V5J=w408-h306-k-no'],
['ChIJyRUQhrh0kFQRW61smDlKCCY',	'Camp Charlie',	'Camp Charlie, 25809 NE 124th St, Duvall, WA 98019, USA',	'(47.705818, -121.99399)',	'https://lh5.googleusercontent.com/p/AF1QipMHsn4qhgagbRJ_vKXQmh1BXHEYveMJNazQZsEH=w408-h725-k-no'],
['ChIJlcFh0L1pkFQR3qC3nrdigwU',	'Wildwood Park',	'Wildwood Park, 7265 87th Ave SE, Mercer Island, WA 98040, USA',	'(47.5365792, -122.22298720000003)',	'https://lh5.googleusercontent.com/p/AF1QipPOCczUJ8B6Uc5KFW_wnGsSYMRr9omhp2SFoSw_=w408-h306-k-no'],
['ChIJiTFqtfxvkFQRzmWYIOuD60g',	'Issaquah Highland Bark Park',	'Issaquah Highland Bark Park, 25th & Natalie Way Issaquah WA 98029, Issaquah, WA 98029, USA',	'(47.554215, -121.99853259999998)',	'https://lh5.googleusercontent.com/p/AF1QipMEkqd-Q6HHNLb3uBqGzVkbUiToZYXoh3rvg0_t=w408-h306-k-no'],
['ChIJ1aqETWD6kFQRp3AWB9YvUSo',	'Allan Yorke Park',	'Allan Yorke Park, 7302 W Tapps Hwy E, Bonney Lake, WA 98391, USA','(47.1910111, -122.16700070000002)',	'https://www.soundsfunmom.com/wp-content/uploads/2017/04/Lake-300x169.png?x53057']];
// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    consoleLog("Connection failed: " . $conn->connect_error);
} 
// drop database
$sql = "drop DATABASE if exists ".$dbname;
if ($conn->query($sql) === TRUE) {
    consoleLog("Database dropped successfully");
} else {
    consoleLog("Error dropping database: " . $conn->error);
}
// Create database
$sql = "CREATE DATABASE ".$dbname;
if ($conn->query($sql) === TRUE) {
    consoleLog("Database created successfully");
} else {
    consoleLog("Error creating database: " . $conn->error);
}
$conn->close();
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    consoleLog("Connection failed: " . $conn->connect_error);
} 
$sql = "CREATE TABLE dog_parks (
table_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
google_id VARCHAR(250),
name VARCHAR(250),
address VARCHAR(250),
location VARCHAR(250),
img_url VARCHAR(250),
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    consoleLog("Table dog_parks created successfully");
} else {
    consoleLog("Error creating table: " . $conn->error);
}
$sql = "CREATE TABLE users (
user_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
first_name VARCHAR(250),
last_name VARCHAR(250),
email VARCHAR(250),
user_name VARCHAR(250),
password VARCHAR(250),
current_dog_park_id VARCHAR(250),
session_id VARCHAR(250),
privacy_mode TINYINT(1) DEFAULT 0 NOT NULL,
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    consoleLog("Table users created successfully");
} else {
    consoleLog("Error creating table: " . $conn->error);
}
$sql = "CREATE TABLE dogs (
dog_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(100),
activity_level VARCHAR(250),
size VARCHAR(250),
img_url VARCHAR(250),
owner_id INT(6),
with_owner TINYINT(1) DEFAULT 1 NOT NULL,
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    consoleLog("Table dogs created successfully");
} else {
    consoleLog("Error creating table: " . $conn->error);
}
$sql = "CREATE TABLE dog_breeds (
dog_breed_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
breed_name VARCHAR(100),
activity_level VARCHAR(250),
size VARCHAR(250),
stock_img_url VARCHAR(250),
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    consoleLog("Table dog_breeds created successfully");
} else {
    consoleLog("Error creating table: " . $conn->error);
}
for($i = 0; $i < count($dogParkData);$i++ ){
$sql = "INSERT INTO dog_parks (google_id, name, address,location, img_url)
VALUES ('".$dogParkData[$i][0]."', '".$dogParkData[$i][1]."','".$dogParkData[$i][2]."','".$dogParkData[$i][3]."','".$dogParkData[$i][4]."')";

if ($conn->query($sql) === TRUE) {
    consoleLog("New Dog Park record created successfully");
} else {
    consoleLog("Error: " . $sql . "<br>" . $conn->error);
}
}
for($i = 0; $i < count($intial_users_data);$i++ ){
$sql = "INSERT INTO users (first_name, last_name, email,user_name,password,current_dog_park_id)
VALUES ('".$intial_users_data[$i][0]."', '".$intial_users_data[$i][1]."','".$intial_users_data[$i][2]."','".$intial_users_data[$i][3]."','".$intial_users_data[$i][4]."','".$intial_users_data[$i][5]."')";

if ($conn->query($sql) === TRUE) {
    consoleLog("New User record created successfully");
} else {
    consoleLog("Error: " . $sql . "<br>" . $conn->error);
}
}
for($i = 0; $i < count($intial_dogs_data);$i++ ){
$sql = "INSERT INTO dogs (name, activity_level, size,img_url,owner_id,with_owner)
VALUES ('".$intial_dogs_data[$i][0]."', '".$intial_dogs_data[$i][1]."','".$intial_dogs_data[$i][2]."','".$intial_dogs_data[$i][3]."','".$intial_dogs_data[$i][4]."','".$intial_dogs_data[$i][5]."')";

if ($conn->query($sql) === TRUE) {
    consoleLog("New dogs record created successfully");
} else {
    consoleLog("Error: " . $sql . "<br>" . $conn->error);
}
}
$conn->close();

?>
