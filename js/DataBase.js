function DataBase(){
    this.reset = function(){
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "ResetDb.php", true);
        xhttp.send();
    }
    this.has_user_name = function(username){
        var deferred = new $.Deferred();
        var data = new Array();
        sql = "SELECT user_name FROM users";
        var returnedValues = sql.substring(sql.indexOf("SELECT")+7,sql.indexOf("FROM"));
              returnedValues = returnedValues.split(" ").join('');
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "dbAccess.php?function=select&sql="+sql+"&returnValues="+returnedValues, true);
        xhttp.send(); 
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                data = this.responseText
                if(data[0] != "$"){
                data = data.split("*");
            }else{
                data = data.slice(1,data.length-1);
                data = data.split("$");
                for(var i = 0; i < data.length; i++){
                    data[i] = data[i].split("*");
                }
            }
            for(var i = 0; i < data.length; i++){
                    console.log(data[i] +" === "+ username);
                    if(data[i] == username){
                        data = true;
                        deferred.resolve(data);
                    }
                }
                data = false;
                deferred.resolve(data);
           }
        };
         return deferred.promise();
    }
    this.select = function(sql){
        var deferred = new $.Deferred();
        var data = new Array();
        var returnedValues = sql.substring(sql.indexOf("SELECT")+7,sql.indexOf("FROM"));
              returnedValues = returnedValues.split(" ").join('');
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "dbAccess.php?function=select&sql="+sql+"&returnValues="+returnedValues, true);
        xhttp.send(); 
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                data = this.responseText
                if(data[0] != "$"){
                data = data.split("*");
                deferred.resolve(data);
            }else{
                data = data.slice(1,data.length-1);
                data = data.split("$");
                for(var i = 0; i < data.length; i++){
                    data[i] = data[i].split("*");
                }
                deferred.resolve(data);
            }
           }
        };
         return deferred.promise();
    }
    this.insert = function(sql){
    var deferred = new $.Deferred();
    var data = "";
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "dbAccess.php?function=update&sql="+sql, true);
    xhttp.send(); 
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            data = this.responseText
            if(data == 'true'){data = true}
            else {data = false}
            deferred.resolve(data);
        }
    };
     return deferred.promise();
}
    this.update = function(sql){
    var deferred = new $.Deferred();
    var data = "";
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "dbAccess.php?function=update&sql="+sql, true);
    xhttp.send(); 
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            data = this.responseText
            if(data == 'true'){data = true}
            else {data = false}
            deferred.resolve(data);
        }
    };
     return deferred.promise();
}
}
