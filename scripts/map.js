var userlocation = {
    lat: 47.7047,
    lng: -122.1670
};
var user;
var userloggedin;
var db = new DataBase();
if(getCookie("session_id") != ""){
   userloggedin = getCookie("session_id");
   console.log("logged in with cookies");
}
else
{
    userloggedin = "";
    console.log("Not signed In");
    }
if(userloggedin == ""){
        $("#settingbtn").remove();
        $("#loginout").replaceWith("<span id='loginout' onClick='signin()' class='w3-bar-item w3-button w3-hover-blue'>Sign In</span>");  
    }

//var userloggedin = "dawkinsarthursir@gmail.com";
//var userloggedin = "sonniesamantha@gmail.com";

var searcharea = 0.5;
var dogparks;
var users;
var dogs;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: userlocation,
        zoom: 12
    });
    infowindow = new google.maps.InfoWindow();
    resetData().done(function (result) {
        fillist();
        createUserMarker(userlocation);
        for(var i = 0; i < dogparks.length; i ++){
            var markerlocation = {
    lat: parseFloat(dogparks[i][3].substring(dogparks[i][3].indexOf("(")+1,dogparks[i][3].indexOf(","))),
    lng: parseFloat(dogparks[i][3].substring(dogparks[i][3].indexOf(",")+1,dogparks[i][3].indexOf(")"))) 
    };
            
            createMarker(markerlocation, dogparks[i][1],dogparks[i][0]);
        }
        if(user.curpark != ""){
            expandpark(user.curpark);
        }
    });
}
function createUserMarker(markerlocation) {
    var markerSize = 45;
    var image = {
          url: '../images/user.png',
          // This marker is 20 pixels wide by 32 pixels high.
          scaledSize: new google.maps.Size(markerSize,markerSize),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(markerSize/2,markerSize)
        };
    var marker = new google.maps.Marker({
    position: markerlocation,
    map: map,
    icon: image
  });
  google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(user.name);
        infowindow.open(map, this);

    });
}
function createMarker(markerlocation, name,google_id) {
    var markerSize = 45;
    var image = {
          url: '../images/MARKER.png',
          // This marker is 20 pixels wide by 32 pixels high.
          scaledSize: new google.maps.Size(markerSize,markerSize),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(markerSize/2,markerSize)
        };
    var marker = new google.maps.Marker({
    position: markerlocation,
    map: map,
    icon: image
  });
  google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(name);
        infowindow.open(map, this);
        expandpark(google_id);

    });
}


function expandpark(google_id){
    if($("#tr"+google_id).siblings().length == 1){
        unexpand();
    }else{
    if($(window).width() < 680){
    window.scrollTo({
    top: 1000,
    behavior: "smooth"
});
}
    $('#output').scrollTop(0);
    var tomove = $("#maintr"+google_id).detach();
    $("#maintable").prepend(tomove);
    $("#currentparkopen").remove();
    $(".parkdisplay").css("border","none");
    $(".parkdisplay").css("border-bottom","gray 1px solid");
    $(".parkdisplay").css("border-radius","0px");
    $("#td"+google_id).css("border","solid black 2px");
    $("#td"+google_id).css("border-radius","7px");
    if(userloggedin == ""){
    $("#tr"+google_id).after("<tr id='currentparkopen'><td id='currentparkopentd' >\n\
        <p style='margin-left: 0'>Sign in to enable check in!</p>\n\
        <button class='w3-button w3-hover-blue w3-round w3-green w3-medium' onClick='signin()'>Sign In</button>\n\
        <button class='w3-button  w3-hover-blue w3-round w3-green w3-medium' id='directionsbtn'   onClick='getdirections(&#39;"+google_id+"&#39;)'>Get Directions</button>\n\
        <p id='amenitiesdisplaytitle'>Features & Amenities</p>\n<div id='amenities'>Coming Soon!</div><p id='dogsdisplaytitle'>Dogs at Park:</p>\n\
        <table id='currentparkopenTable'></table></td></tr>");       
    }
    else if(user.curpark === google_id){
        $("#tr"+google_id).after("<tr id='currentparkopen'><td id='currentparkopentd' >\n\
        <button class='w3-button  w3-hover-green w3-round w3-gray w3-medium' id='checkoutbtn'  onClick='checkout(&#39;"+google_id+"&#39;)'>Checked Out</button>\n\
        <button class='w3-button  w3-hover-blue w3-round w3-green w3-medium' id='directionsbtn'   onClick='getdirections(&#39;"+google_id+"&#39;)'>Get Directions</button>\n\
        <p id='amenitiesdisplaytitle'>Features & Amenities</p>\n<div id='amenities'>Coming Soon!</div><p id='dogsdisplaytitle'>Dogs at Park:</p>\n\
        <table id='currentparkopenTable'></table></td></tr>");
    }
    else
    {
        $("#tr"+google_id).after("<tr id='currentparkopen'><td id='currentparkopentd' >\n\
        <button class='w3-button  w3-hover-blue w3-round w3-green w3-medium' id='checkinbtn'  onClick='checkin(&#39;"+google_id+"&#39;)'>Check In</button>\n\
        <button class='w3-button  w3-hover-blue w3-round w3-green w3-medium' id='directionsbtn'   onClick='getdirections(&#39;"+google_id+"&#39;)'>Get Directions</button>\n\
        <p id='amenitiesdisplaytitle'>Features & Amenities</p>\n<div id='amenities'>Coming Soon!</div><p id='dogsdisplaytitle'>Dogs at Park:</p>\n\
        <table id='currentparkopenTable'></table></td></tr>");
    }
        var dogsatparkbool = false;
        var privatedogsatpark = false;
        var privatedogcount = 0;
        for(var u = 0; u < users.length; u++){
            if(google_id == users[u][6]){
                for(var d = 0; d < dogs.length; d++){
                    if(users[u][0] == dogs[d][2] && dogs[d][3] == "1" && users[u][8] == "0"){
                        dogsatparkbool = true;
                        $("#currentparkopenTable").append("<tr><td class='dogdisplay'><img src='"+dogs[d][5]+
                    "' class='w3-col m4 s4'><div class='w3-col m8 s8'>\n\
            <p>"+dogs[d][0]+"</p><p>Activity: "+dogs[d][1]+"</p><p>Size: "+dogs[d][4]+
                                            "</p></div></td></tr>");
                }
                if(users[u][0] == dogs[d][2] && dogs[d][3] == "1" && users[u][8] == "1"){
                    privatedogsatpark = true;
                    privatedogcount++;
                }
            }
        }
    }
    if(privatedogsatpark && !dogsatparkbool){
        $("#currentparkopenTable").append("<tr><td id='nodogs'><h4>"+privatedogcount+" private dog profiles are checked in</h4></td></tr>");
    }else if(!dogsatparkbool){
        $("#currentparkopenTable").append("<tr><td id='nodogs'><h4>There are no dogs checked in!</h4></td></tr>");
    }else if(privatedogsatpark){
        $("#currentparkopenTable").append("<tr><td id='nodogs'><h4>"+privatedogcount+" private dog profiles are checked in</h4></td></tr>");
    }
}
}
///https://www.google.com/maps/search/?api=1&query=1200%20Pennsylvania%20Ave%20SE%2C%20Washington%2C%20District%20of%20Columbia%2C%2020003
//https://www.google.com/maps/dir/?api=1&origin=Space+Needle+Seattle+WA&destination=Pike+Place+Market+Seattle+WA&travelmode=bicycling
//lat: 47.6740,
 //   lng: -122.1215
 //https://www.google.com/maps/dir/?api=1&origin=47.6740,-122.1215&destination=Pike+Place+Market+Seattle+WA&travelmode=driving
/*
//examples
db.update("UPDATE users SET last_name='Donald' WHERE user_id=1").done(function (result) {
  //there is no error checking or checking for duplicate entrys that all needs to be done sepratly
  //result returns true or false here if entry was successfulk or not
     console.log(result);
})



db.select("SELECT user_id ,first_name,last_name, email, user_name, password, current_dog_park_id FROM users").done(function (result) {
    //do something with the result array
    //its up to you to know if result will be 1d or 2d based off of query sent
    //this query would return a 2d array 99 indexs long and 3 wide
    
     console.log(result);
})


db.select("SELECT name FROM dog_parks").done(function (result) {
    //do something with the result array
    //its up to you to know if result will be 1d or 2d based off of query sent
    //this query would return a 2d array 99 indexs long and 3 wide
    
     console.log(result);
})

db.has_user_name("freddy234").done(function (result) {
    if(!result){
        db.insert("INSERT INTO users (first_name, last_name, email, user_name, password) VALUES ('John', 'Doe', 'john@example.com', 'freddy234', 'sdfger456')").done(function (result) {
})
    }
    
        
    
     
})*/
function signin(){
    window.location = "login.html";
}
function getdirections(google_id){
    var address;
    for(var p = 0; p < dogparks.length; p++){
        if(dogparks[p][0] == google_id){
            address = dogparks[p][2];
        }
    }
    var orginString = "https://www.google.com/maps/dir/?api=1&origin=";
    var destinationString= "&destination=";
    var endString = "&travelmode=driving";
    address = address.replace(/ /g, "+");
    address = address.replace(/,/g, "%2C");
    var url = orginString+userlocation.lat+","+userlocation.lng+destinationString+address+endString;
    window.location = url; 
}
function checkout(google_id){
    db.update("UPDATE users SET current_dog_park_id = '' WHERE email = '"+user.email+"'").done(function (result) {  
    console.log("checked out "+result);
    resetData().done(function (result) {
        fillist();
        expandpark(google_id);
    }); 
});

}
function checkin(google_id){
    db.update("UPDATE users SET current_dog_park_id = '"+google_id+"' WHERE email ='"+user.email+"'").done(function (result) {
        console.log("checked in "+result);
     resetData().done(function (result) {
        fillist();
        expandpark(google_id);
    
    });
});
 
}
function fillist(){
    
    $("#maintable").children().remove();
    for(var p = 0; p < dogparks.length; p++){
        
         $("#maintable").append("<tr id='maintr"+dogparks[p][0]+"'><td class='parkdisplay'id='td"+dogparks[p][0]+"'>\n\
    <table><tr class='parkdisplayinnertr' id='tr"+dogparks[p][0]+"' onClick='expandpark(&#39;"+dogparks[p][0]+"&#39;)'>\n\
    <td class='w3-row'><img src='"+dogparks[p][4]+"' class='w3-col m4 s4'><div class='w3-col m8 s8'>\n\
            <p>"+dogparks[p][1]+"</p><p>"+dogparks[p][2].substring(dogparks[p][2].indexOf(",")+1)+"</p><p class='usersdisplaynum' id='users"+dogparks[p][0]+"'>\n\
</p></div></td></tr></table></td></tr>"); 
        var dogcount = 0;
        var usercount = 0;
        for(var u = 0; u < users.length; u++){
            if(users[u][6] === dogparks[p][0]){
            usercount++;
            for(var d = 0; d < dogs.length; d++){
                if(users[u][0] === dogs[d][2] && dogs[d][3] == "1"){
                    dogcount++;
                }
            }
        }
        }
        $("#users"+dogparks[p][0]+"").html("Users: "+usercount+" Dogs: "+dogcount);
}
}
//resetData().done(function (result) {console.log(result)});
function resetData(){
    var deferred = new $.Deferred();
    var data = false;
    db.select("SELECT google_id,name,address,location,img_url FROM dog_parks").done(function (result) {
        dogparks = result;
        //console.log(dogparks);
        db.select("SELECT user_id,first_name,last_name,email,user_name,password,current_dog_park_id,session_id,privacy_mode FROM users").done(function (result) {
            users = result;
            //console.log(users);
            db.select("SELECT name,activity_level,owner_id,with_owner,size,img_url FROM dogs").done(function (result) {
            dogs = result;
            //console.log(dogs);
            for(var i = 0; i < users.length; i ++){
            if(userloggedin != ""){
                if(users[i][7] == userloggedin){
                    user = new User(users[i][3],userlocation,users[i][1]+" "+users[i][2],users[i][4],users[i][0],users[i][6],users[i][8]);
                }
            }
            else
            {
                user = new User("Not Signed In",userlocation,"Not Signed In","Not Signed In","Not Signed In","Not Signed In");
            }
        }
            data = true;
            deferred.resolve(data);
        });
        });  
});
     return deferred.promise();
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function unexpand() {
    $("#currentparkopen").remove();
    $(".parkdisplay").css("border","none");
    $(".parkdisplay").css("border-bottom","gray 1px solid");
    $(".parkdisplay").css("border-radius","0px");
}
function logOut() {
    setCookie("session_id", "" , 1);
    userloggedin = "";
    resetData().done(function (result) {
        fillist();
        $("#settingbtn").remove();
        $("#loginout").replaceWith("<span id='loginout' onClick='signin()' class='w3-bar-item w3-button w3-hover-blue'>Sign In</span>");  
    });
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}