var userloggedin;
var users;
var dogs;
var user;
var activitylevelcounter = 0;
var activitylevel = ["Low","Medium","High"];
var sizecounter = 0;
var size = ["Xsmall","Small","Medium","Large","Xlarge"];
var dogimagescounter=0;
var dogimages = [
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12213218/German-Shepherd-on-White-00-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12232815/Glen-of-Imaal-Terrier-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12232719/Golden-Retriever-On-White-05-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12232605/Great-Dane-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12212157/Great-Pyrenees-on-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/20110654/Hamiltonstovare-on-White-011-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/21231052/Harrier-Slide-12-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12204819/Kai-Ken-on-White-02-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12231449/Kuvasz-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12231213/Lancashire-Heeler-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12231013/Maltese-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12210850/Nederlandse-Kooikerhondje-on-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12230343/Norwegian-Elkhound-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12230240/Norwich-Terrier-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12225945/Pekingese-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12225919/Pembroke-Welsh-Corgi-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12214239/Pharaoh-Hound-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12232547/Great-Dane-On-White-07.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001815/Alaskan-Malamute-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001724/American-Eskimo-Dog-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001904/Akita-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001519/American-Staffordshire-Terrier-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001408/Australian-Cattle-Dog-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001049/Basset-Hound-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001000/Beagle-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13000440/Bernese-Mountain-Dog-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13000032/Bolognese-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235921/Border-Terrier-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235957/Border-Collie-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13000208/Bloodhound-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13000046/Boerboel-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235809/Boston-Terrier-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12212631/Borzoi-on-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235639/Boxer-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235526/Briard-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235434/Broholmer-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12212238/Brussels-Griffon-on-White-06-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235344/Bull-Terrier-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12212353/Bullmastiff-on-White-05-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234710/Chihuahua-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234759/Chesapeake-Bay-Retriever-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/15141010/Cesky-Terrier-on-White-011-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234623/Chinese-Shar-Pei-On-White-02-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234523/Chow-Chow-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234244/Collie-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234147/Czechoslovakian-Vlcak-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12234026/Dalmatian-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12205855/Danish-Swedish-Farmdog-on-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12233900/Doberman-Pinscher-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12233808/Dogue-de-Bordeaux-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12225627/Pomeranian-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12224728/Samoyed-On-White-03-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12224843/Saint-Bernard-On-White-01-400x267.jpg",
    "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12224224/Siberian-Husky-On-White-03-400x267.jpg"
];
var db = new DataBase();
if(getCookie("session_id") != ""){
   userloggedin = getCookie("session_id");
   console.log("logged in with cookies");
}else {
    alert("You are not logged in!");
    window.location = "login.html";
}

resetData().done(function (result) {
filldoglist(user.userid);
 });

function filldoglist(user_id){
resetData().done(function (result) {
    $(".dogDisplayDiv").remove();
    for(var d = 0; d < dogs.length; d++){
        if(dogs[d][2] == user_id && dogs[d][3] == 1){
            $("#displaydogs").prepend("<div class=' dogDisplayDiv'>\n\
                <img id='dogimg' src='"+dogs[d][5]+"'>\n\
                <img  src='../images/closeblack.png' onClick='removedog(&#39;"+dogs[d][0]+"&#39;,&#39;"+dogs[d][6]+"&#39;)' id='removedog'>\n\
                <p>Name: "+dogs[d][0]+"</br>Activity: "+dogs[d][1]+"</br>Size: "+dogs[d][4]+"</br>\n\
<button onClick='setinactive(&#39;"+dogs[d][6]+"&#39;)' class='w3-btn w3-green w3-round' >With You</button></p></div>\n\
                ");
        }
        if(dogs[d][2] == user_id && dogs[d][3] == 0){
            $("#displaydogs").prepend("<div class=' dogDisplayDiv'>\n\
                <img id='dogimg' src='"+dogs[d][5]+"'>\n\
                <img  src='../images/closeblack.png' onClick='removedog(&#39;"+dogs[d][0]+"&#39;,&#39;"+dogs[d][6]+"&#39;)' id='removedog'>\n\
                <p>Name: "+dogs[d][0]+"</br>Activity: "+dogs[d][1]+"</br>Size: "+dogs[d][4]+"</br>\n\
                <button onClick='setactive(&#39;"+dogs[d][6]+"&#39;)' class='w3-btn w3-gray w3-round' >Not With You</button></p>\n\
                </div>\n\
                ");
        }
    }
    });
}
function resetData(){
    var deferred = new $.Deferred();
    var data = false;
        db.select("SELECT user_id,first_name,last_name,email,user_name,password,current_dog_park_id,session_id FROM users").done(function (result) {
            users = result;
            //console.log(users);
            db.select("SELECT name,activity_level,owner_id,with_owner,size,img_url,dog_id FROM dogs").done(function (result) {
            dogs = result;
            //console.log(dogs);
            var loggedintrue = false;
            for(var i = 0; i < users.length; i ++){
            if(users[i][7] == userloggedin){
                user = new User(users[i][3],"userlocation",users[i][1]+" "+users[i][2],users[i][4],users[i][0],users[i][6],users[i][8]);
                $("#curuser").text(user.username);
                $("#curemail").text(user.email);
                loggedintrue = true;   
            }
        }
        if(!loggedintrue){
            alert("You Are not logged in!");
                window.location = "login.html";
            
        }
        db.select("SELECT privacy_mode FROM users WHERE user_id = "+user.userid).done(function (result) {
                    if(result[0] == 1){
                        $('#privacycheck').prop('checked', true);
                    }else {
                        $('#privacycheck').prop('checked', false);
                    }  
                });
            data = true;
            deferred.resolve(data);
        });
        });  
     return deferred.promise();
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setactive(dog_id){
    db.update("UPDATE dogs SET with_owner = 1 WHERE dog_id = "+dog_id).done(function (result) {
        filldoglist(user.userid);
     console.log(result);
})
}
function setinactive(dog_id){
    db.update("UPDATE dogs SET with_owner = 0 WHERE dog_id = "+dog_id).done(function (result) {
        filldoglist(user.userid);
     console.log(result);
})
}
function swapimg(){
    dogimagescounter++;
    if(dogimagescounter == dogimages.length){
        dogimagescounter = 0;
    }
    $("#imagediv").html("<img id='imageselected' src='"+dogimages[dogimagescounter]+"'>");
    
    
}
function newimgleft(){
    dogimagescounter--;
    if(dogimagescounter <= 0){
        dogimagescounter = dogimages.length-1;
    }
    $("#imagediv").html("<img id='imageselected' src='"+dogimages[dogimagescounter]+"'>");
}
function swapactivity(){
    activitylevelcounter++;
    if(activitylevelcounter == activitylevel.length){
        activitylevelcounter = 0;
    }
    $("#newdogactivity").html("Activity: "+activitylevel[activitylevelcounter]);
    
    
}
function swapsize(){
    sizecounter++;
    if(sizecounter == size.length){
        sizecounter = 0;
    }
    $("#newdogsize").html("Size : "+size[sizecounter]);
    
    
}
function adddog(){
    
    db.insert("INSERT INTO dogs( name, activity_level, size,img_url, owner_id,with_owner)VALUES ('"+$("#newdogname").val()+"','"
                +activitylevel[activitylevelcounter]+"','"+size[sizecounter]+"','"+$("#imageselected").attr("src")+"','"+user.userid+"',1);").done(function (result) {

            console.log("Record inserted "+result);
            $(".adddogarea").remove();
            filldoglist(user.userid);
            $("#displaydogs").append("<div class='w3-row' onClick='plusdog()' id='plusdiv'>\n\
                        <h1><img src='../images/plus.jpg'>add a dog</h1>\n\
                    </div>");
    });
}
function plusdog(){
    $("#plusdiv").remove();
    $("#displaydogs").append("<div class='w3-row dogadd w3-container adddogarea'><img  src='../images/closeblack.png' onClick='closenewdog()' id='closenewdog'>\n\<div onClick='swapimg()' id='imagediv'></div>\n\
                        <button onClick='newimgleft()' class='w3-btn w3-green w3-small w3-round' id='newimageright'>Previous</button>\n\
                          <button class='w3-btn w3-green w3-small w3-round' onClick='swapimg()' id='newimageleft'>Next</button>"+
                    "<p>Name: <input size='25' type='text' id='newdogname' required=''></p>\n\
                    <button class='w3-btn w3-green w3-small w3-round' onClick='swapactivity()' id='newdogactivitybutton'>Select</button><p id='newdogactivity'>Activity: "+activitylevel[activitylevelcounter]+"</p>\n\
                     <button onClick='swapsize()' class='w3-btn w3-green w3-small w3-round' id='newdogsizebutton'>Select</button><p id='newdogsize'>Size: "+size[sizecounter]+"</p>"+
                    "<button class=' w3-block  w3-btn w3-center w3-green w3-large w3-round' onclick='adddog()' id='newdogsizebutton' style='width:99%'>Submit</button>"+
                    "</div>");
    $("#imagediv").html("<img id='imageselected' src='"+dogimages[dogimagescounter]+"'>");
}
function closenewdog(){
 
    $(".adddogarea").remove();
            $("#displaydogs").append("<div class='w3-row' onClick='plusdog()' id='plusdiv'>\n\
                        <h1><img src='../images/plus.jpg'>add a dog</h1>\n\
                    </div>");
    
    
}
function logOut() {
    setCookie("session_id", "" , 30);
    setCookie("email", "" , 30);
    window.location = "login.html";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function removedog(dog_name,dog_id){
    if(confirm("Are you sure that you want to remove "+dog_name+" from your account?")){
    db.update("DELETE FROM dogs WHERE dog_id = "+dog_id).done(function (result) {
            console.log(result);   
            filldoglist(user.userid);
    });
}
    }
function removeaccount(){
    if(confirm("Are you sure that you want to delete your account?")){
    db.update("DELETE FROM users WHERE user_id = "+user.userid).done(function (result) {
            console.log(result);   
            alert("Your Accout has been deleted");
            window.location = "../index.html";
    });
}
    }
$("#deleteaccount").submit(function(event){
event.preventDefault();
console.log("FUCK");
db.select("SELECT password FROM users WHERE user_id ="+user.userid).done(function (result) {
    console.log(result);
        if($("#deletepassword").val() == result[0]){
            $("#deletePassworderror").text("");
            console.log("resultdoublefuck");
            removeaccount();
        }else {
            $("#deletePassworderror").text("Password is incorrect");
            console.log("resultdouawdawdawduck");
        }
});
});
$("#privacycheck").change(function(){
    console.log("resultawdawdawd");
   if(this.checked){
       db.update("UPDATE users SET privacy_mode = 1 WHERE user_id = "+user.userid).done(function (result) {     
     console.log(result);
});
   }else {
       db.update("UPDATE users SET privacy_mode = 0 WHERE user_id = "+user.userid).done(function (result) {     
     console.log(result);
});
   } 
});
function changepassword(password){
    db.update("UPDATE users SET password = '"+password+"' WHERE user_id = "+user.userid).done(function (result) {     
     console.log(result);
});
}
var numbers = /^[0-9]+$/;
var letters = /^[a-zA-Z]+$/;
$("#changepassword").submit(function(event){
event.preventDefault();
console.log("FUCK");
db.select("SELECT password FROM users WHERE user_id = "+user.userid).done(function (result) {
    console.log(result);
        if($("#oldpassword").val() == result[0]){
            $("#oldPasswordLoginError").text("");
        var num = false;
        var let = false;
        for(var i = 0; i < $("#newpassword").val().length; i++){
            if($("#newpassword").val()[i].match(numbers)){
                num = true;
            }
            if($("#newpassword").val()[i].match(letters)){
                let = true;
            }
        }
            if(num && let){
            if($("#newpassword").val().length >= 8 && $("#newpassword").val().length <= 12){
            console.log("resultdoublefuck");
            changepassword($("#newpassword").val());
            $("#oldpassword").val("");
            $("#newpassword").val("");
            alert("password has been changed!");
            //window.location = "AccountManagment.html";
        }else {
            $("#newPasswordLoginError").text("Password must be between 8 and 12 characters");
            console.log("resultdouawdawdawduck");
                }
            }
        else {
            $("#newPasswordLoginError").text("Password must contian 1 letter and 1 number");
            console.log("resultdouawdawdawduck");
                }
        }
        else {
            $("#oldPasswordLoginError").text("Password is incorrect");
            console.log("resultdouawdawdawduck");
        }
});
});